 #!/bin/bash


directorio="$1"



# Verifica si el directorio existe y si es un directorio válido
if [ ! -d "$directorio" ]; then                                  #-d devuelve si el archivo existe  (! -d 1) si no existe entonces:
echo "$directorio no es un directorio válido."
exit 1
fi


#inciso a
echo "----------------------------------------------------"
echo "			DIRECTORIOS			  "
echo "NUMERO DE DIRECTORIOS:"
ND=$(ls -la "$directorio" | grep '^d'  | wc -l)
echo "$ND"
echo "----------------------------------------------------";

#inciso b
echo "----------------------------------------------------"
echo "			ARCHIVOS"
echo " NUMERO DE ARCHIRVOS "
NA=$(ls -la "$directorio" |grep '^-' | wc -l)
echo "$NA"
echo "----------------------------------------------------"
#inciso c
echo "---------------------------------------------------"
echo "                       OTROS"
NNR=$(ls -la "$directorio" |grep -vE '^d|^-' | wc -l)
echo "$NNR"
echo "----------------------------------------------------"

