# EJERICICIO10

#Script de Conteo de Archivos y Directorios
Este script de Bash está diseñado para realizar las siguientes tareas en un directorio especificado:

Verificar que el argumento pasado en la línea de comandos sea un directorio válido.
Contar la cantidad de directorios que contiene el directorio especificado.
Contar la cantidad de archivos regulares que contiene el directorio especificado.
Contar la cantidad de archivos que no son regulares ni directorios en el directorio especificado.

#Instrucciones para Ejecutar el Script:

1 . abrir la terminal de linux
2 . dirigirse al directorio donde se encuentra el script
3 . ejecutar el script pasandole como argumento el directorio por ejemplo:
       ./ejercicio10.sh ~/Documentos


